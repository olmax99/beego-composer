package conf

import (
	"fmt"
	"strings"

	beego "github.com/beego/beego/v2/server/web"
)

func BeeConf(ini ...string) map[string]string {
	m := make(map[string]string, len(ini))
	for _, i := range ini[:] {
		entry, err := beego.AppConfig.String(i)
		if err != nil {
			fmt.Printf("ERROR [*] beeConf().. %v", err)
		}
		if strings.Contains(i, "::") {
			s := strings.Split(i, "::")
			m[s[1]] = entry
		} else {
			m[i] = entry
		}
	}
	return m
}
