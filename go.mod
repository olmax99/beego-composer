module gitlab.com/olmax99/beego-composer

go 1.16

require (
	github.com/beego/beego/v2 v2.0.1
	github.com/lib/pq v1.10.2
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
