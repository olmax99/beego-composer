package routers

import (
	beego "github.com/beego/beego/v2/server/web"
	"gitlab.com/olmax99/beego-composer/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/register", &controllers.LoginController{}, "get:RegisterView;post:Register")

}
