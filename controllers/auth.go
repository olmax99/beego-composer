package controllers

import (
	"encoding/hex"
	"fmt"

	beego "github.com/beego/beego/v2/server/web"
	pk "gitlab.com/olmax99/beego-composer/utils/pbkdf2"

	"log"

	"github.com/beego/beego/v2/adapter/orm"
	"github.com/beego/beego/v2/core/validation"
	"github.com/beego/beego/v2/server/web/context"
	"gitlab.com/olmax99/beego-composer/conf"
	"gitlab.com/olmax99/beego-composer/models"
)

func (this *LoginController) LoginView() {
	this.TplName = "authentication/login.html"
}

type LoginController struct {
	beego.Controller
}

func (this *LoginController) RegisterView() {
	this.TplName = "authentication/register.html"
}

func (this *LoginController) Register() {
	firstname := this.GetString("firstname")
	lasttname := this.GetString("lastname")
	email := this.GetString("lastname")
	username := this.GetString("username")
	password1 := this.GetString("password1")
	password2 := this.GetString("password2")
	test := models.RegisterForm{Username: username, Password1: password1, Password2: password2, FirstName: firstname, LastName: lasttname, Email: email}

	valid := validation.Validation{}
	b, err := valid.Valid(&test)
	if err != nil {
		log.Printf("ERROR [*] Register validation.. %v", err)
	}
	if !b {
		for _, err := range valid.Errors {
			log.Println(err.Key, err.Message)
		}
	} else {
		h := pk.HashPassword(password1)

		user := new(models.User)
		user.Username = username
		user.FirstName = firstname
		user.LastName = lasttname
		user.Email = email
		user.Password = hex.EncodeToString(h.Hash) + hex.EncodeToString(h.Salt)

		o := orm.NewOrm()
		o.Using("default")

		_, err := o.Insert(user)
		if err != nil {
			log.Printf("ERROR [*] Register insert.. %v", err)
		}

		this.Redirect("/", 302)
	}
	this.TplName = "authentication/register.html"
}

func (this *LoginController) Login() {
	username := this.GetString("username")
	password := this.GetString("password")

	var user models.User
	if VerifyUser(&user, username, password) {
		beeC := conf.BeeConf(
			"sessionName",
		)
		v := this.GetSession(beeC["sessionName"])
		if v == nil {
			this.SetSession(beeC["sessionName"], user.Id)
		}
		log.Printf("DEBUG [*] Login Ctx.Input.CruSession: %#v", this.Ctx.Input.CruSession)
		log.Printf("DEBUG [*] Login Ctx.Input: %#v", this.Ctx.Input)
		log.Printf("DEBUG [*] Login Ctx.Request: %#v", this.Ctx.Request)
		this.Redirect("/app/welcome", 302)

	} else {
		log.Println("DEBUG [*] Login VerifyUser failed..")
		this.Redirect("/register", 302)
	}

}

func (this *LoginController) Logout() {
	beeC := conf.BeeConf(
		"sessionName",
	)

	this.DelSession(beeC["sessionName"])
	this.Redirect("/login", 302)
}

func (this *LoginController) AppView() {
	this.TplName = "private/welcome.html"
}

func VerifyUser(user *models.User, username, password string) (success bool) {
	var x pk.PasswordHash
	x.Hash = make([]byte, 32)
	x.Salt = make([]byte, 16)
	// named return parameters implicitely initiated as '0'
	// search user by username or email
	if err := HasUser(user, username); err == nil {
		x.Hash, err = hex.DecodeString(user.Password[:64])
		if err != nil {
			fmt.Println("ERROR [*] VerifyUser Hash.. ", err)
		}
		x.Salt, err = hex.DecodeString(user.Password[64:])
		if err != nil {
			fmt.Println("ERROR [*] VerifyUser Salt.. ", err)
		}
	}
	if pk.MatchPassword(password, &x) {
		success = true
	}
	if !success {
		log.Println("ERROR [*] VerifyUser failed..")
	}
	return
}

func HasUser(user *models.User, username string) error {
	qs := orm.NewOrm()
	user.Username = username
	err := qs.Read(user, "Username")
	if err == nil {
		return nil
	}
	return err
}

// customize filters for fine grain authorization
var FilterUser = func(ctx *context.Context) {
	beeC := conf.BeeConf(
		"sessionName",
	)
	// TODO evaluate if this is method is restrictive enough..
	// Do not authorize when:
	// 1. a session does not exists
	// 2. the request does not come from Request.RequestURI "/login"
	// 3. the request does not come from Request.RequestURI "/register"
	_, ok := ctx.Input.Session(beeC["sessionName"]).(int)
	if !ok && ctx.Input.URI() != "/login" && ctx.Input.URI() != "/register" {
		fmt.Printf("DEBUG [*] FilterUser ctx.Input.CruSession: %#v", ctx.Input.CruSession)
		ctx.Redirect(302, "/login")
	}
}
