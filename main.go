package main

import (
	"log"
	"time"

	"github.com/beego/beego/v2/adapter/orm"
	beego "github.com/beego/beego/v2/server/web"
	_ "github.com/beego/beego/v2/server/web/session/mysql" // used for all session stores
	_ "github.com/beego/beego/v2/server/web/session/redis"
	_ "github.com/lib/pq"
	_ "gitlab.com/olmax99/beego-composer/models"
	_ "gitlab.com/olmax99/beego-composer/routers"
)

func init() {
	// Development Settings, adjust for production
	// mysql / sqlite3 / postgres driver registered by default already
	maxIdle := 10 // (optional):  set maximum idle connections
	maxConn := 10 // (optional):  set maximum connections (go >= 1.2)

	orm.RegisterDriver("postgres", orm.DRPostgres)

	//                    db alias  drivername
	orm.RegisterDataBase("default", "postgres", "user=beego password=super_secret dbname=composer sslmode=disable", maxIdle, maxConn)
	orm.DefaultTimeLoc = time.UTC

}

func main() {
	// set to make internal template compatible with most front ends
	// i.e. Angular, Polymer, etc
	beego.BConfig.WebConfig.TemplateLeft = "<<<"
	beego.BConfig.WebConfig.TemplateRight = ">>>"

	alias := "default"
	// Whether to drop table and re-create.
	force := true
	// Print log.
	verbose := true
	// Error.
	err := orm.RunSyncdb(alias, force, verbose)
	if err != nil {
		log.Println(err)
	}
	beego.Run()
}
