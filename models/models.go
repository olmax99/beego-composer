package models

import (
	"time"

	"github.com/beego/beego/v2/adapter/orm"
)

type User struct {
	Id          int       `orm:"column(id);auto"`
	FirstName   string    `orm:"column(firstname);size(50)"`
	LastName    string    `orm:"column(lastname);size(50)"`
	Username    string    `orm:"column(username);"`
	Email       string    `orm:"column(email);unique"`
	Password    string    `orm:"column(password);size(128)"`
	CreatedDate time.Time `orm:"column(created_date);type(timestamp);auto_now_add"`
}

func init() {
	orm.RegisterModel(new(User))
}
